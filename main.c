#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>
#include "generacja.h"
#include "zestaw_zasad.h"

int main(int argc, char *argv[])
{
    char tablica[40][81];
    
    generacja(atoi(argv[1]), tablica, 40);
    zestaw_zasad(tablica, atoi(argv[2]), atoi(argv[3])); 

    return 0;
}
